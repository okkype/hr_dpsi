{
    'name': "HR Travel Management",
    'summary': "Employee Travel Management",
    'description': "Employee Travel Management",
    'author': "Rozaq",
    'website': "",
    'category': 'Human Resources',
    'version': '0.1',
    'depends': [
        'hr',
        'decimal_precision',
        'product',
        'hr_expense'
    ],
    'data': [
        'security/ir.model.access.csv',
		"views/hr_travel_request.xml",
    ],
	"installable": True
}