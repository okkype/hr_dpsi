'''
Created on 20 Apr 2018

@author: okky
'''

from openerp import tools, models, fields, api, _, exceptions
from openerp.exceptions import except_orm

class HREmployee(models.Model):
    _name = "hr.employee"
    _inherit = "hr.employee"
    
    @api.one
    def _get_logs(self):
        self.auditlog = self.env['auditlog.log'].search([('model_id.model', '=', self._name), ('res_id', '=', self.id)])
        
    auditlog = fields.Many2many('auditlog.log', string='Logs', compute=_get_logs)

class HRContract(models.Model):
    _name = "hr.contract"
    _inherit = "hr.contract"
    
    @api.one
    def _get_logs(self):
        self.auditlog = self.env['auditlog.log'].search([('model_id.model', '=', self._name), ('res_id', '=', self.id)])
        
    auditlog = fields.Many2many('auditlog.log', string='Logs', compute=_get_logs)

class HRHolidays(models.Model):
    _name = "hr.holidays"
    _inherit = "hr.holidays"
    
    @api.cr_uid_ids_context
    def holidays_first_validate(self, cr, uid, ids, context=None):
        obj_emp = self.pool.get('hr.employee')
        ids2 = obj_emp.search(cr, uid, [('user_id', '=', uid)])
        manager = ids2 and ids2[0] or False
        rms = self.browse(cr, uid, ids)
        for rm in rms:
            real_mgr = rm.employee_id and rm.employee_id.parent_id and rm.employee_id.parent_id.user_id and rm.employee_id.parent_id.user_id.id
            if real_mgr and (uid != real_mgr):
                raise except_orm(_('Warning'), _('You are not allowed to approve this document.'))
        print manager
        self.holidays_first_validate_notificate(cr, uid, ids, context=context)
        return self.write(cr, uid, ids, {'state':'validate1', 'manager_id': manager})

    @api.cr_uid_ids_context
    def holidays_validate(self, cr, uid, ids, context=None):
        obj_emp = self.pool.get('hr.employee')
        ids2 = obj_emp.search(cr, uid, [('user_id', '=', uid)])
        manager = ids2 and ids2[0] or False
        print manager
        self.write(cr, uid, ids, {'state':'validate'})
        data_holiday = self.browse(cr, uid, ids)
        for record in data_holiday:
            if record.double_validation:
                rms = self.browse(cr, uid, ids)
                for rm in rms:
                    real_mgr = rm.employee_id and rm.employee_id.parent_id and rm.employee_id.parent_id.parent_id and rm.employee_id.parent_id.parent_id.user_id and rm.employee_id.parent_id.parent_id.user_id.id
                    if real_mgr and (uid != real_mgr):
                        raise except_orm(_('Warning'), _('You are not allowed to approve this document.'))
                self.write(cr, uid, [record.id], {'manager_id2': manager})
            else:
                rms = self.browse(cr, uid, ids)
                for rm in rms:
                    real_mgr = rm.employee_id and rm.employee_id.parent_id and rm.employee_id.parent_id.user_id and rm.employee_id.parent_id.user_id.id
                    if real_mgr and (uid != real_mgr):
                        raise except_orm(_('Warning'), _('You are not allowed to approve this document.'))
                self.write(cr, uid, [record.id], {'manager_id': manager})
            if record.holiday_type == 'employee' and record.type == 'remove':
                meeting_obj = self.pool.get('calendar.event')
                meeting_vals = {
                    'name': record.name or _('Leave Request'),
                    'categ_ids': record.holiday_status_id.categ_id and [(6, 0, [record.holiday_status_id.categ_id.id])] or [],
                    'duration': record.number_of_days_temp * 8,
                    'description': record.notes,
                    'user_id': record.user_id.id,
                    'start': record.date_from,
                    'stop': record.date_to,
                    'allday': False,
                    'state': 'open',  # to block that meeting date in the calendar
                    'class': 'confidential'
                }   
                # Add the partner_id (if exist) as an attendee             
                if record.user_id and record.user_id.partner_id:
                    meeting_vals['partner_ids'] = [(4, record.user_id.partner_id.id)]
                    
                ctx_no_email = dict(context or {}, no_email=True)
                meeting_id = meeting_obj.create(cr, uid, meeting_vals, context=ctx_no_email)
                self._create_resource_leave(cr, uid, [record], context=context)
                self.write(cr, uid, ids, {'meeting_id': meeting_id})
            elif record.holiday_type == 'category':
                emp_ids = obj_emp.search(cr, uid, [('category_ids', 'child_of', [record.category_id.id])])
                leave_ids = []
                batch_context = dict(context, mail_notify_force_send=False)
                for emp in obj_emp.browse(cr, uid, emp_ids, context=context):
                    vals = {
                        'name': record.name,
                        'type': record.type,
                        'holiday_type': 'employee',
                        'holiday_status_id': record.holiday_status_id.id,
                        'date_from': record.date_from,
                        'date_to': record.date_to,
                        'notes': record.notes,
                        'number_of_days_temp': record.number_of_days_temp,
                        'parent_id': record.id,
                        'employee_id': emp.id
                    }
                    leave_ids.append(self.create(cr, uid, vals, context=batch_context))
                for leave_id in leave_ids:
                    # TODO is it necessary to interleave the calls?
                    for sig in ('confirm', 'validate', 'second_validate'):
                        self.signal_workflow(cr, uid, [leave_id], sig)
        return True
