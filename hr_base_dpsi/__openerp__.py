# -*- coding: utf-8 -*-
{
    'name': "HR Base DPSI",
    'description': "HR Base DPSI",
    'version': '0.1',
    'author': 'PT.SISI, Okky Permana',
    'license': 'LGPL-3',
    'category': 'hr',
    'depends': [
        'auditlog',
        'hr_holidays',
        'hr_employee_info',
        'hr_travel_management'
    ],
    'data': [
        'report/kertas.xml',
        'report/template/cuti_template.xml',
        'report/template/cuti_thn_template.xml',
        'report/cuti.xml',
        'view/hr.xml',
    ],
    'qweb' : [],
    'auto_install': False,
    'application': True,
    'installable': True
}
